package designpatterns.chain.step5;


import designpatterns.chain.step1.ServiceLevel;
import designpatterns.chain.step1.ServiceRequest;
import designpatterns.chain.step2.SupportServiceItf;
import designpatterns.chain.step3.SupportService;
import designpatterns.chain.step4.*;

public class TestChainOfResponsibility {
	public static void main(String[] args) 
	{

		SupportService supportService1 = new SupportService();

		SupportServiceItf frontDeskSupport = new FrontDeskSupport();
		SupportServiceItf supervisorSupport = new SupervisorSupport();
		SupportServiceItf managerSupport = new ManagerSupport();
		SupportServiceItf directorSupport = new DirectorSupport();


		((DirectorSupport) directorSupport).setNext(null);
		((ManagerSupport) managerSupport).setNext(directorSupport);
		((SupervisorSupport) supervisorSupport).setNext(managerSupport);
		((FrontDeskSupport) frontDeskSupport).setNext(supervisorSupport);
		supportService1.setHandler(frontDeskSupport);


		ServiceRequest request = new ServiceRequest();
//		request.setType(ServiceLevel.LEVEL_ONE);
////		supportService.handleRequest(request);
//		supportService1.handleRequest(request);
//		System.out.println(request.getConclusion());
//
//		request = new ServiceRequest();
//		request.setType(ServiceLevel.LEVEL_THREE);
////		supportService.handleRequest(request);
//		supportService1.handleRequest(request);
//		System.out.println(request.getConclusion());
//
		request = new ServiceRequest();
		request.setType(ServiceLevel.INVALID_REQUEST);
//		supportService.handleRequest(request);
		supportService1.handleRequest(request);
		System.out.println(request.getConclusion());
	}
}