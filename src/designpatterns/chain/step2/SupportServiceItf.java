package designpatterns.chain.step2;


import designpatterns.chain.step1.ServiceRequest;

public interface SupportServiceItf
{
	public void handleRequest(ServiceRequest request);
}