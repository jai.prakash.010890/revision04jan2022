package designpatterns.command.step2;

public class Fan {

    public  void start() {
        System.out.println("Fan started");
    }

    public void stop() {
        System.out.println("Fan stopped");
    }
}
