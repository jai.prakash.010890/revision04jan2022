package designpatterns.command.step2;

/**
 * It is a receiver
 */
public class Light {
    public  void turnOn() {
        System.out.println("Light is on");
    }

    public void turnOff() {
        System.out.println("Light is off");
    }
}
