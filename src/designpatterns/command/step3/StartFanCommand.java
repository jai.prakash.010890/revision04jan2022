package designpatterns.command.step3;

import designpatterns.command.step1.ICommand;
import designpatterns.command.step2.Fan;

public class StartFanCommand implements ICommand {
 
    Fan fan;
 
    public StartFanCommand(Fan fan) {
        super();
        this.fan = fan;
    }
 
    public void execute() {
        System.out.println("starting Fan.");
        fan.start();
    }
}