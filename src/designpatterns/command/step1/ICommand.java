package designpatterns.command.step1;

@FunctionalInterface
public interface ICommand {

    public void execute();
}
