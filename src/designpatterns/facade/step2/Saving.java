package designpatterns.facade.step2;


import designpatterns.facade.step1.IAccount;

import java.math.BigDecimal;

public class Saving implements IAccount {
    public Saving(BigDecimal initAmount) {
    }

    @Override
    public void deposit(BigDecimal amount) {

    }

    @Override
    public void withdraw(BigDecimal amount) {

    }

    @Override
    public void transfer(IAccount toAccount, BigDecimal amount) {

    }

    @Override
    public int getAccountNumber() {
        return 0;
    }
}
