package designpatterns.facade.step2;



import designpatterns.facade.step1.IAccount;

import java.math.BigDecimal;

public class Chequing implements IAccount {

    private BigDecimal initAmount;

    public Chequing(BigDecimal initAmount) {
        this.initAmount = initAmount;
    }

    @Override
    public void deposit(BigDecimal amount) {

    }

    @Override
    public void withdraw(BigDecimal amount) {

    }

    @Override
    public void transfer(IAccount toAccount, BigDecimal amount) {

    }

    @Override
    public int getAccountNumber() {
        return 0;
    }
}
