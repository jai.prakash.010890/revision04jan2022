package designpatterns.facade.step1;

import java.math.BigDecimal;

//create the interface

public interface IAccount {

    void deposit(BigDecimal amount);
    void withdraw(BigDecimal amount);
    void transfer(IAccount toAccount, BigDecimal amount);
    int getAccountNumber();

}
