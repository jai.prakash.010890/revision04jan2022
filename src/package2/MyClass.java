package package2;

public class MyClass<T> {

    T i;

    MyClass(T i){
        this.i = i;
    }

    void doPrint(){
        System.out.println(i.getClass().getName());
    }
}
