package package2;

public class TestGenerics {

    public static void main(String[] args) {
        MyClass<Integer> myClass = new MyClass<>(5);
        MyClass<Double> myClass1 = new MyClass<>(5.0);

        myClass.doPrint();
        myClass1.doPrint();

        // talk about passing multiple arguments
    }
}
