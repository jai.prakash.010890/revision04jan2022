package fp.package1;

import fp.package2.PaymentType;
import fp.package2.Purchase;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Purchase order1 = new Purchase(1,1000, PaymentType.DEBIT);
        Purchase order2 = new Purchase(2, 2000, PaymentType.CREDIT);
        Purchase order3 = new Purchase(3, 3000,PaymentType.DEBIT);
        Purchase order4 = new Purchase(4,400, PaymentType.DEBIT);

        List<Purchase> orders = new ArrayList<>();
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);

        List<Purchase> lst = orders.stream().filter(o->o.getOrderAmount() > 5000).collect(Collectors.toList());

        Optional<Purchase> po = orders.stream().findAny();

        Purchase po1 = po.filter(o-> o.getOrderAmount() >100).get();




    }
}
