package fp.package1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Ex1 {

    public static void main(String[] args) {
        List<Integer> lst = new ArrayList<>();
        lst.add(1);
        lst.add(2);
        lst.add(3);
        lst.add(1);
        lst.add(2);
        lst.add(4);
        lst.add(5);
        lst.add(9);
        lst.add(8);
//        printNumbers(lst);
//        printNumbersFI(lst);
    }

    private static void printNumbers(List<Integer> lst) {

        for(Integer i: lst){
            if(i %2 == 0)
                System.out.println(i);
        }
    }

    private boolean isEven(int num){
        return num %2 ==0;
    }

    private  void printNumbersFI(List<Integer> lst) {

        extracted(lst, (num) -> num%2==0);
        extracted(lst, (num) -> num%2!=0);

    }

    private void extracted(List<Integer> lst, Predicate<Integer> evenPredicate) {
        lst.stream()
                .filter(evenPredicate)
                .forEach(System.out::println);
    }
}

/**
 * Lambads => Logic
 *
 * operations on it
 *  intermediate // predicate
 *  ternary =>
 */