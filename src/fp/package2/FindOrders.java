package fp.package2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FindOrders {
    public static void main(String[] args) {
        Purchase order1 = new Purchase(1,1000, PaymentType.DEBIT);
        Purchase order2 = new Purchase(2, 2000, PaymentType.CREDIT);
        Purchase order3 = new Purchase(3, 3000,PaymentType.DEBIT);
        Purchase order4 = new Purchase(4,400, PaymentType.DEBIT);

        List<Purchase> orders = new ArrayList<>();
        orders.add(order1);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);

        /* *
        find the id of transaction sorted in desceding order for debit cards
        3,1,4
        *  iterate, check debit card, comparator and sort,

        * */

//        Collections.sort(orders, new Comparator<Purchase>() {
//            @Override
//            public int compare(Purchase o1, Purchase o2) {
//                return o2.getOrderAmount()-o1.getOrderAmount();
//            }
//        });
//
//        List<Integer> lst = new ArrayList<>();
//
//        for(Purchase p: orders){
//            if(p.getPaymentType() == PaymentType.DEBIT)
//                lst.add(p.getOrderId());
//        }

//        System.out.println(lst);



        List<?> ordersLst = orders.stream()
                .filter((ord) -> ord.getPaymentType()== PaymentType.DEBIT)
                .sorted(Comparator.comparing(Purchase::getOrderAmount).reversed())
                .map(Purchase::getOrderId)
                .collect(Collectors.toList());

        System.out.println(ordersLst);
    }
}
