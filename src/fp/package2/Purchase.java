package fp.package2;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor
public class Purchase {
    private int orderId;
    private int orderAmount;
    private PaymentType paymentType;

}
