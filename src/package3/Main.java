package package3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Animal a = new Feline();
        f(a);

        f(new Tiger());
        f(new Feline());

        List<Feline> felines = new ArrayList<Feline>();

        Feline[] felines1= new Feline[8];


    }

    public static void f(Animal x){

    }

    public static void fList(List<Animal> la){
        //iterate through array and call method

        // why java does not allow this bcz we can add new dog obj to list
    }

    public static void fArray(Animal[] la){
        //iterate through array and call method
        // generics is what compiler knows
    }

    public static void fListCovariance(List<? extends Animal> la){
        //iterate through array and call method

//        la.add(new Dog()); not allowed as we cannot determine
        // why java does not allow this bcz we can add new dog obj to list
    }

    // contravariance: able to pass generics which pass
    // can we pass list of felines
    // so it accepts not only tiger but anything which is super of Tiger
    public static void contraList(List<? super Tiger> input){
        Object o = input.get(0);
//        Animal ani = input.get(0);
    }
}
